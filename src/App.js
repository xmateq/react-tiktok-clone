import React from 'react';
import "./App.css";
import Video from './Video';

export default function App() {
  return (
    <div className="app">
      <div className="app__videos">
        <Video
          url="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
          channel="testChannel1"
          description="testDescription1"
          song="testSong1"
          likes={178}
          shares={347}
          messages={46} 
        />
        <Video
          url="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
          channel="testChannel2"
          description="testDescription2"
          song="testSong2"
          likes={4534}
          shares={56}
          messages={34} 
         />
        <Video />
        <Video />
      </div>
    </div>
  );
}
